package com.example.application.security;

import com.example.application.views.loginview.LoginView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import org.springframework.stereotype.Component;

/**
 * As Vaadin is a single page app there is no browser refresh wnen navigating between view
 * Spring Security is wired into the Vaadin navigation system below:
 */

@Component
public class ConfigureUIServiceInitListener implements VaadinServiceInitListener {

// listen for the initialization of the UI and add a listener before every view transition
@Override
public void serviceInit(ServiceInitEvent event) {
	event.getSource().addUIInitListener(uiEvent -> {
				final UI ui = uiEvent.getUI();
				ui.addBeforeEnterListener(this::authenticateNavigation);
			});
}

// reroute all requests to login if user not logged in.
private void authenticateNavigation(BeforeEnterEvent event) {
	if(!LoginView.class.equals(event.getNavigationTarget())
	&& !SecurityUtils.isUserLoggedIn()) {
		event.rerouteTo(LoginView.class);
	}
}
}
