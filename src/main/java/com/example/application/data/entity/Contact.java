package com.example.application.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Contact implements Comparable<Contact> {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@NotEmpty
private String firstName;

@NotEmpty
private String lastName;

@ManyToOne
@JoinColumn(name = "company_id")
@NotNull
@JsonIgnoreProperties({"employees"})
private Company company;

@NotNull
@ManyToOne
private Status status;

@Email
@NotEmpty
private String email;

@Override
public String toString() {
	return "Contact{" +
		       "firstName='" + firstName + '\'' +
		       ", lastName='" + lastName + '\'' +
		       ", email='" + email + '\'' +
		       '}';
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public Company getCompany() {
	return company;
}

public void setCompany(Company company) {
	this.company = company;
}

public Status getStatus() {
	return status;
}

public void setStatus(Status status) {
	this.status = status;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

@Override
public int compareTo(Contact o) {
	return 0;
}
}
