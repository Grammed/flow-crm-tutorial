package com.example.application.data.entity;

import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Company {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@Formula("(SELECT COUNT(c.id) FROM Contact c WHERE c.company_id = id)")
private int employeeCount;

@NotBlank
private String name;

@OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
private List<Contact> employees = new LinkedList<>();

public int getEmployeeCount(){
	return employeeCount;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public List<Contact> getEmployees() {
	return employees;
}

public void setEmployees(List<Contact> employees) {
	this.employees = employees;
}
}
