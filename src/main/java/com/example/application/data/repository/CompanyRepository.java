package com.example.application.data.repository;

import com.example.application.data.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Long> {
	@Query(
		"SELECT c FROM Company c WHERE LOWER(c.name) LIKE LOWER(concat('%', :searchTerm, '%'))")
List<Company> companyNameContains(@Param("searchTerm") String searchTerm);

}
