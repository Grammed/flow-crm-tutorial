package com.example.application.data.service;

import com.example.application.data.entity.Company;
import com.example.application.data.entity.Contact;
import com.example.application.data.entity.Status;
import com.example.application.data.repository.CompanyRepository;
import com.example.application.data.repository.ContactRepository;
import com.example.application.data.repository.StatusRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CrmService {
	private final ContactRepository CONTACT_REPO;
	private final CompanyRepository COMPANY_REPO;
	private final StatusRepository STATUS_REPO;

	public CrmService(ContactRepository CONTACT_REPO, CompanyRepository COMPANY_REPO, StatusRepository STATUS_REPO) {
		this.CONTACT_REPO = CONTACT_REPO;
		this.COMPANY_REPO = COMPANY_REPO;
		this.STATUS_REPO = STATUS_REPO;
	}

	public List<Contact> returnAllContactsOrSearch(String searchTerm) {
		if (searchTerm == null || searchTerm.isEmpty()) {
			return CONTACT_REPO.findAll();
		} else
			return CONTACT_REPO.firstOrLastNameContains(searchTerm);
	}

	public Long countContacts() {
		return CONTACT_REPO.count();
	}

	public void deleteContact(Contact contact) {
		CONTACT_REPO.delete(contact);
	}

	public void saveContact(Contact contact) {
		if (contact == null)
			System.err.println("Contact is null. Are you sure that you have connected your form to the application?");
		CONTACT_REPO.save(contact);
	}

	public void deleteCompany(Company company) {
		COMPANY_REPO.delete(company);
	}

public void saveCompany(Company company) {
	if (company == null)
		System.err.println("Company is null. Are you sure that you have connected your form to the application?");
	COMPANY_REPO.save(company);
}

	public List<Company>	returnAllCompaniesOrSearch(String searchTerm) {
		if (searchTerm == null || searchTerm.isEmpty()) {
			return COMPANY_REPO.findAll();
		}
		return COMPANY_REPO.companyNameContains(searchTerm);
	}

	public List<Company> findAllCompanies() {
		return COMPANY_REPO.findAll();
	}

	public List<Status> findAllStatuses() {
		return STATUS_REPO.findAll();
	}


}
