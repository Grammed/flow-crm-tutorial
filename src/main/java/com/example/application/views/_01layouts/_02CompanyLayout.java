package com.example.application.views._01layouts;

import com.example.application.views.company.CompanyDetailView;
import com.example.application.views.company.CompanyView;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;

@ParentLayout(value = MainLayout.class)
public class _02CompanyLayout extends VerticalLayout implements RouterLayout {

	public _02CompanyLayout() {
		createRoutes();
	}

	private void createRoutes() {
		RouterLink companyLink = new RouterLink("Companies", CompanyView.class);
		RouterLink companyDetailLink = new RouterLink("Detail-view", CompanyDetailView.class);
		companyLink.setHighlightCondition(HighlightConditions.sameLocation());
		companyDetailLink.setHighlightCondition(HighlightConditions.sameLocation());

	}






}
