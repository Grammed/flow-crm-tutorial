package com.example.application.views._01layouts;

import com.example.application.security.SecurityService;
import com.example.application.views.company.CompanyDetailView;
import com.example.application.views.company.CompanyView;
import com.example.application.views.contact.ContactView;
import com.example.application.views.dashboardview.DashboardView;
import com.example.application.views.play.PlayView;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.Theme;

@Theme(themeFolder = "flowcrmtutorial")
public class MainLayout extends AppLayout {
	private final SecurityService securityService;

	public MainLayout(SecurityService securityService) {
		this.securityService = securityService;
		createHeader();
		createDrawer();
	}

	private void createHeader() {
		H1 logo = new H1("Vaadin CRM");
		logo.addClassNames("text-1", "m-m");

		Button logout = new Button("Log out", e -> securityService.logout());


		HorizontalLayout header = new HorizontalLayout(
			new DrawerToggle(),
			logo,
			logout
		);

		header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
		header.setWidth("100%");
		header.expand(logo);
		header.addClassNames("py-0", "px-m");
		addToNavbar(header);
	}

	// AppLayout has a draw automatically so there is no need to 'add' it
	private void createDrawer() {
		RouterLink contactLink = new RouterLink("Contacts", ContactView.class);
		RouterLink companyLink = new RouterLink("Companies", CompanyView.class);
		RouterLink companyDetailLink = new RouterLink("Detail-view", CompanyDetailView.class);
		RouterLink dashLink = new RouterLink("Dashboard", DashboardView.class);
		RouterLink playLink = new RouterLink("Play", PlayView.class);
		//RouterLink allCompanyLinks = new RouterLink("Companies", _02CompanyLayout.class);

		contactLink.setHighlightCondition(HighlightConditions.sameLocation());
		companyLink.setHighlightCondition(HighlightConditions.sameLocation());
		companyDetailLink.setHighlightCondition(HighlightConditions.sameLocation());
	//	allCompanyLinks.setHighlightCondition(HighlightConditions.sameLocation());
		dashLink.setHighlightCondition(HighlightConditions.sameLocation());
		playLink.setHighlightCondition(HighlightConditions.sameLocation());

		addToDrawer(new VerticalLayout(
			contactLink,
	//		allCompanyLinks,
			companyLink,
			companyDetailLink,
			dashLink,
			playLink
		));

	}






}
