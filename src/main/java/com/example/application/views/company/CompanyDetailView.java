
package com.example.application.views.company;

import com.example.application.data.entity.Company;
import com.example.application.data.entity.Contact;
import com.example.application.data.service.CrmService;
import com.example.application.views._01layouts.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.util.List;

import static java.util.Objects.isNull;

@Route(value = "company-detail", layout = MainLayout.class)
@PageTitle("Companies | Vaadin CRM")
public class CompanyDetailView extends VerticalLayout {
private Grid<Company> grid = new Grid<>(Company.class);
private Grid<Contact> contactAsDetailGrid = new Grid<>();
private Company selectedCompany = new Company();
private TextField filterText = new TextField();
private final CrmService CRM_SERVICE;

public CompanyDetailView(CrmService CRM_SERVICE) {
	this.CRM_SERVICE = CRM_SERVICE;

	addClassName("company-view");
	setSizeFull();
	configureGrid();
	add(getToolbar(), getContent());

	add(getContent());
	updateList();
	closeEditor();

}

private void detailRenderer() {
	grid.setItemDetailsRenderer(
		new ComponentRenderer<>(company -> getDetailsView()));
}

private HorizontalLayout populateDetailLayout(Company company) {
	List<Contact> returnedEmployees = company.getEmployees();
	HorizontalLayout wasAGridLastWeek = new HorizontalLayout();
	VerticalLayout firstName = new VerticalLayout();
	VerticalLayout lastName = new VerticalLayout();
	VerticalLayout email = new VerticalLayout();


	for (Contact emp: returnedEmployees) {
		VerticalLayout newFirstName = new VerticalLayout();
		VerticalLayout newLastName = new VerticalLayout();
		VerticalLayout newEmail = new VerticalLayout();
		newFirstName.add(emp.getFirstName());
		newLastName.add(emp.getLastName());
		newEmail.add(emp.getEmail());
		firstName.add(newFirstName);
		lastName.add(newLastName);
		email.add(newEmail);
	}

	wasAGridLastWeek.addAndExpand(firstName, lastName, email);

	wasAGridLastWeek.setAlignItems(Alignment.START);

	return wasAGridLastWeek;

}

private VerticalLayout getDetailsView() {
	VerticalLayout main = new VerticalLayout();
	HorizontalLayout details  = populateDetailLayout(selectedCompany);
	Div headerDiv = new Div();
	HorizontalLayout header = new HorizontalLayout();
	headerDiv.add("Contacts made at " + selectedCompany.getName() + " are:");
	header.add(headerDiv);
	main.add(header, details);

	return main;
}


private void closeEditor() {
	removeClassName("editing");
}



protected void updateList() {
	grid.setItems(CRM_SERVICE.returnAllCompaniesOrSearch(filterText.getValue()));
}



private Component getContent() {
	HorizontalLayout content = new HorizontalLayout(grid);
	content.setFlexGrow(2, grid);
	//content.setFlexGrow(1, companyForm);
	content.addClassName("content");
	content.setSizeFull();
	return content;
}

private void finishedEditing() {
}

private void populateGrid() {
	grid.setItems(CRM_SERVICE.returnAllCompaniesOrSearch(null));
}

private void configureGrid() {
	grid.addClassName("company-grid");
	grid.setSizeFull();
	grid.setColumns("name", "employeeCount");
	for (Grid.Column<Company> col : grid.getColumns()) {
		col.setAutoWidth(true);
	}

	grid.asSingleSelect().addValueChangeListener(valueChangeEvent -> selectedCompany = valueChangeEvent.getValue());
	grid.asSingleSelect().addValueChangeListener(valueChangeEvent -> detailRenderer());
}



private void editCompany(Company value) {
	if (isNull(value)) {
	}else {
	}
}



private HorizontalLayout getToolbar() {

	filterText.setPlaceholder("Filter by name...");
	filterText.setClearButtonVisible(true);
	filterText.setValueChangeMode(ValueChangeMode.LAZY);
	filterText.addValueChangeListener(valueChangeStoppedTyping -> updateList());

	//Button doSomethingNewButton = new Button("Something New");
	//doSomethingnewButton.addClickListener(click ->doSomethingNew());

	HorizontalLayout toolbar = new HorizontalLayout(filterText);
	toolbar.addClassName("toolbar");
	return toolbar;
}

/*private void doSomethingNew() {
	grid.asSingleSelect().clear();
	editCompany(new Company());

}*/
}
