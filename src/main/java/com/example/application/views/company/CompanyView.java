
package com.example.application.views.company;

import com.example.application.data.entity.Company;
import com.example.application.data.service.CrmService;
import com.example.application.views._01layouts.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import static java.util.Objects.isNull;

@Route(value = "companies", layout = MainLayout.class)
@PageTitle("Companies | Vaadin CRM")
public class CompanyView extends VerticalLayout {
private Grid<Company> grid = new Grid<>(Company.class);
private TextField filterText = new TextField();
private CompanyForm companyForm;
private final CrmService CRM_SERVICE;

public CompanyView(CrmService CRM_SERVICE) {
	this.CRM_SERVICE = CRM_SERVICE;

	addClassName("company-view");
	setSizeFull();
	configureGrid();
	configureCompanyForm();

	add(getToolbar(), getContent());

	add(getContent());
	updateList();
	closeEditor();
}

private void closeEditor() {
	companyForm.setCompany(null);
	companyForm.setVisible(false);
	removeClassName("editing");
}



protected void updateList() {
	grid.setItems(CRM_SERVICE.returnAllCompaniesOrSearch(filterText.getValue()));
}



private Component getContent() {
	HorizontalLayout content = new HorizontalLayout(grid, companyForm);
	content.setFlexGrow(2, grid);
	content.setFlexGrow(1, companyForm);
	content.addClassName("content");
	content.setSizeFull();
	return content;
}


private void configureCompanyForm() {

	companyForm = new CompanyForm();
	companyForm.addListener(CompanyForm.SaveEvent.class, saveEvent -> saveCompany(saveEvent));
	companyForm.addListener(CompanyForm.DeleteEvent.class, deleteEvent -> deleteCompany(deleteEvent));
	companyForm.addListener(CompanyForm.CloseEvent.class, closeEvent ->  finishedEditing());
	companyForm.setWidth("25em");
}

private void deleteCompany(CompanyForm.DeleteEvent event) {
	CRM_SERVICE.deleteCompany(event.getCompany());
	populateGrid();
	finishedEditing();
}


private void saveCompany(CompanyForm.SaveEvent event) {
	CRM_SERVICE.saveCompany(event.getCompany());
	populateGrid();
	finishedEditing();
}

private void finishedEditing() {
	companyForm.setCompany(null);
	companyForm.setVisible(false);
	companyForm.removeClassName("editing");
}



private void populateGrid() {
	grid.setItems(CRM_SERVICE.returnAllCompaniesOrSearch(null));
}



private void configureGrid() {
	grid.addClassName("company-grid");
	grid.setSizeFull();
	grid.setColumns("name", "employeeCount");
	for (Grid.Column<Company> col : grid.getColumns()) {
		col.setAutoWidth(true);
	}

	grid.asSingleSelect().addValueChangeListener(valueChangeEvent ->
		                                             editCompany(valueChangeEvent.getValue()));
}


private void editCompany(Company value) {
	if (isNull(value)) {
		closeEditor();
	}else {
		companyForm.setCompany(value);
		companyForm.setVisible(true);
		companyForm.addClassName("editing");
	}
}



private HorizontalLayout getToolbar() {

	filterText.setPlaceholder("Filter by name...");
	filterText.setClearButtonVisible(true);
	filterText.setValueChangeMode(ValueChangeMode.LAZY);
	filterText.addValueChangeListener(valueChangeStoppedTyping -> updateList());

	Button addCompanyButton = new Button("Add company");
	addCompanyButton.addClickListener(click ->addCompany());

	HorizontalLayout toolbar = new HorizontalLayout(filterText, addCompanyButton);
	toolbar.addClassName("toolbar");
	return toolbar;
}

private void addCompany() {
	grid.asSingleSelect().clear();
	editCompany(new Company());

}
}
