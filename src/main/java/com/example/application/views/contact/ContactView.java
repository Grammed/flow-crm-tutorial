package com.example.application.views.contact;

import com.example.application.data.entity.Contact;
import com.example.application.data.service.CrmService;
import com.example.application.views._01layouts.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import static java.util.Objects.isNull;

@Route(value = "contacts", layout = MainLayout.class)
@PageTitle("Contacts | Vaadin CRM")
public class ContactView extends VerticalLayout {
	private Grid<Contact> grid = new Grid<>(Contact.class);
	private TextField filterText = new TextField();
	private ContactForm contactForm;
	private final CrmService CRM_SERVICE;

	public ContactView(CrmService CRM_SERVICE) {
		this.CRM_SERVICE = CRM_SERVICE;

		addClassName("contact-view");
		setSizeFull();
		configureGrid();
		configureContactForm();

		add(getToolbar(), getContent());
		updateList();
		closeEditor();
	}

private void closeEditor() {
		contactForm.setContact(null);
		contactForm.setVisible(false);
		removeClassName("editing");
}


protected void updateList() {
		grid.setItems(CRM_SERVICE.returnAllContactsOrSearch(filterText.getValue()));
}


private Component getContent() {
		HorizontalLayout content = new HorizontalLayout(grid, contactForm);
		content.setFlexGrow(2, grid);
		content.setFlexGrow(1, contactForm);
		content.addClassName("content");
		content.setSizeFull();
		return content;
}


private void configureContactForm() {

		contactForm = new ContactForm(CRM_SERVICE.findAllCompanies(), CRM_SERVICE.findAllStatuses());
		contactForm.addListener(ContactForm.SaveEvent.class, saveEvent -> saveContact(saveEvent));
		contactForm.addListener(ContactForm.DeleteEvent.class, deleteEvent -> deleteContact(deleteEvent));
		contactForm.addListener(ContactForm.CloseEvent.class, closeEvent ->  finishedEditing());
		contactForm.setWidth("25em");
}

private void deleteContact(ContactForm.DeleteEvent event) {
		CRM_SERVICE.deleteContact(event.getContact());
		populateGrid();
		finishedEditing();
}

private void saveContact(ContactForm.SaveEvent event) {
		CRM_SERVICE.saveContact(event.getContact());
		populateGrid();
		finishedEditing();
}

private void finishedEditing() {
		contactForm.setContact(null);
		contactForm.setVisible(false);
		contactForm.removeClassName("editing");
}

private void populateGrid() {
		grid.setItems(CRM_SERVICE.returnAllContactsOrSearch(null));
}


private void configureGrid() {
		grid.addClassName("contact-grid");
		grid.setSizeFull();
		grid.setColumns("firstName", "lastName", "email");
		grid.addColumn(contact -> contact.getStatus().getName()).setHeader("Status");
		grid.addColumn(contact -> contact.getCompany().getName()).setHeader("Company");
		for (Grid.Column<Contact> col : grid.getColumns()) {
		col.setAutoWidth(true);
		}

		grid.asSingleSelect().addValueChangeListener(valueChangeEvent ->
			                                            editContact(valueChangeEvent.getValue()));
	}

private void editContact(Contact value) {
		if (isNull(value)) {
			closeEditor();
		}else {
			contactForm.setContact(value);
			contactForm.setVisible(true);
			contactForm.addClassName("editing");
		}
}


private HorizontalLayout getToolbar() {

		filterText.setPlaceholder("Filter by name...");
		filterText.setClearButtonVisible(true);
		filterText.setValueChangeMode(ValueChangeMode.LAZY);
		filterText.addValueChangeListener(valueChangeStoppedTyping -> updateList());

		Button addContactButton = new Button("Add contact");
		addContactButton.addClickListener(click ->addContact());

		HorizontalLayout toolbar = new HorizontalLayout(filterText, addContactButton);
		toolbar.addClassName("toolbar");
		return toolbar;
	}

private void addContact() {
		grid.asSingleSelect().clear();
		editContact(new Contact());

}
}
