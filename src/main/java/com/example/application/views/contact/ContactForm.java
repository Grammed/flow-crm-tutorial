package com.example.application.views.contact;

import com.example.application.data.entity.Company;
import com.example.application.data.entity.Contact;
import com.example.application.data.entity.Status;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class ContactForm extends FormLayout {
private TextField firstName = new TextField("First name:");
private TextField lastName = new TextField("Last name:");
private EmailField email = new EmailField("Email");
private ComboBox<Status> status = new ComboBox<>("Status");
private ComboBox<Company> company = new ComboBox<>("Company");

private Button save = new Button("Save");
private Button delete = new Button("Delete");
private Button close = new Button("Cancel");

private Binder<Contact> binder = new BeanValidationBinder<>(Contact.class);

private Contact contact;

public ContactForm(List<Company> companies, List<Status> statuses) {
	addClassName("contact-form");
	binder.bindInstanceFields(this);

	company.setItems(companies);
	company.setItemLabelGenerator(company -> company.getName());
	status.setItems(statuses);
	status.setItemLabelGenerator(status -> status.getName());

	add(
		firstName,
		lastName,
		email,
		company,
		status,
		createButtonsLayout());
}

private HorizontalLayout createButtonsLayout() {
	save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
	delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
	close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

	save.addClickShortcut(Key.ENTER);
	close.addClickShortcut(Key.ESCAPE);

	save.addClickListener(saveEvent -> validateAndSave());
	delete.addClickListener(deleteEvent -> fireEvent(new DeleteEvent(this, contact)));
	close.addClickListener(closeEvent -> fireEvent(new CloseEvent(this)));

	binder.addStatusChangeListener(statusChanged -> save.setEnabled(binder.isValid()));
	return new HorizontalLayout(save, delete, close);
}

private void validateAndSave() {
	try{
		binder.writeBean(contact); // write bean back to original contact.
		fireEvent(new SaveEvent(this, contact));
	} catch (ValidationException e) {
		e.printStackTrace();
	}
}

void setContact(Contact contact) {
	this.contact = contact;
	binder.readBean(contact);
}


// Broadcast events heard by Listeners in the View
public static abstract class ContactFormEvent extends ComponentEvent<ContactForm> {
	private Contact contact;

	protected ContactFormEvent(ContactForm source, Contact contact) {
		super(source, false);
		this.contact = contact;
	}

	public Contact getContact() {
		return this.contact;
	}
}

public static class SaveEvent extends ContactFormEvent {
	SaveEvent(ContactForm source, Contact contact) {
		super(source, contact);
	}

}

public static class DeleteEvent extends ContactFormEvent {
	DeleteEvent(ContactForm source, Contact contact) {
		super(source, contact);
	}

}

public static class CloseEvent extends ContactFormEvent {
	CloseEvent(ContactForm source) {
		super(source, null);
	}
}

// eventBus is used to register the custom event types.
public <T extends ComponentEvent<?>>Registration
addListener(Class<T> eventType,
            ComponentEventListener<T> listener) {
	return getEventBus().addListener(eventType, listener);
}



}
