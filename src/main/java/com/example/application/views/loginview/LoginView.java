package com.example.application.views.loginview;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route("login")
@PageTitle("user|userpass || Vaadin CRM")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

	private final LoginForm login = new LoginForm();

	public LoginView() {
		addClassName("login-view");
		setSizeFull();
		setAlignItems(Alignment.CENTER);
		setJustifyContentMode(JustifyContentMode.CENTER);

		login.setAction("login");
		H1 header = new H1("Vaadin CRM");
		header.addClassName("login-header");

		add(new H1("Vaadin CRM"), login);
	}

@Override
public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
		// inform user about error:
	if(beforeEnterEvent.getLocation()
	.getQueryParameters()
	.getParameters()
	.containsKey("error")) {
		login.setError(true);
	}

}
}
