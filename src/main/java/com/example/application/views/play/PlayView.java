package com.example.application.views.play;

import com.example.application.data.entity.Contact;
import com.example.application.data.service.CrmService;
import com.example.application.views._01layouts.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "play", layout = MainLayout.class)
@PageTitle("play | Components")
public class PlayView extends VerticalLayout {
private Grid<Contact> grid = new Grid<>(Contact.class);
private TextField filterText = new TextField();
// 1.SETVISIBLE
Span label = new Span("Original label");
Button makeSpanVisible;
// 2.Enabling&Disabling
FormLayout form = new FormLayout();

private final CrmService CRM_SERVICE;

public PlayView(CrmService CRM_SERVICE) {
	this.CRM_SERVICE = CRM_SERVICE;

	addClassName("list-view");
	setSizeFull();
	configureGrid();

	add(getToolbar(), getSetVisible(), getForm(), getContent());
	updateList();
}


//region 1.SETVISIBLE
//  if a component is set invisible before rendering, its corresponding element is not created in the DOM
private HorizontalLayout getSetVisible() {
	configureSpan();
	configureButton();

	HorizontalLayout setVisible = new HorizontalLayout(label, makeSpanVisible);
	setVisible.addClassName("setVisible"); // could not 'setFlexDirection'
	setVisible.setId("label-set-as-hidden-in-dom");
	setVisible.setAlignItems(Alignment.CENTER);
	return setVisible;
}

private void configureButton() {
	makeSpanVisible = new Button("MkSpanVisible", evt -> {
		label.setVisible(true);
	});

}

private void configureSpan() {
	label.setVisible(false);
	label.setText("Updated label");
}


// endregion
// region 2.ENABLING&DISABLING
private FormLayout getForm() {
	form.add(configureFields(), configureSubmitButton());
	form.setEnabled(false);

	return form;
}

private VerticalLayout configureSubmitButton() {
	VerticalLayout subButton = new VerticalLayout();
	subButton.setClassName("verticalL-submitbutton");

	Button submitButton = new Button("Submit");

	subButton.setAlignSelf(Alignment.CENTER); // want centre vertically  too == not this
	subButton.setAlignItems(Alignment.CENTER); // works
	subButton.add(submitButton);

	return subButton;

}

private VerticalLayout configureFields() {
	VerticalLayout fields = new VerticalLayout();

	TextField name = new TextField("Name");
	EmailField email = new EmailField("Email");

	fields.setAlignItems(Alignment.CENTER);
	fields.add(name, email);

	return fields;

}
// end region


protected void updateList() {
	grid.setItems(CRM_SERVICE.returnAllContactsOrSearch(filterText.getValue()));
}


private Component getContent() {
	HorizontalLayout content = new HorizontalLayout(grid);
	content.setFlexGrow(2, grid);
	//content.setFlexGrow(1, getSetVisible());
	content.addClassName("content");
	content.setSizeFull();
	return content;
}


private void populateGrid() {
	grid.setItems(CRM_SERVICE.returnAllContactsOrSearch(null));
}


private void configureGrid() {
	grid.addClassName("5col-grid");
	grid.setId("contact-grid");
	grid.setSizeFull();
	grid.setColumns("firstName", "lastName", "email");
	grid.addColumn(contact -> contact.getStatus().getName()).setHeader("Status");
	grid.addColumn(contact -> contact.getCompany().getName()).setHeader("Company");
	for (Grid.Column<Contact> col : grid.getColumns()) {
		col.setAutoWidth(true);
	}
}




private HorizontalLayout getToolbar() {

	filterText.setPlaceholder("Filter by name...");
	filterText.setClearButtonVisible(true);
	filterText.setValueChangeMode(ValueChangeMode.LAZY);
	filterText.addValueChangeListener(valueChangeStoppedTyping -> updateList());


	HorizontalLayout toolbar = new HorizontalLayout(filterText);
	toolbar.addClassName("toolbar");
	return toolbar;
}




}