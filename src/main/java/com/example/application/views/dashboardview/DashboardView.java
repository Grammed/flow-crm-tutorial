package com.example.application.views.dashboardview;


import com.example.application.data.service.CrmService;
import com.example.application.views._01layouts.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.ChartType;
import com.vaadin.flow.component.charts.model.DataSeries;
import com.vaadin.flow.component.charts.model.DataSeriesItem;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@Route(value = "", layout = MainLayout.class)
@PageTitle("Dashboard | Vaadin CRM")
public class DashboardView extends VerticalLayout {
	private final CrmService CRM_SERVICE;

	public DashboardView(CrmService crmService) {
		this.CRM_SERVICE = crmService;
		addClassName("dashboard-view");
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		add(getContactStats(), getCompaniesChart());
	}

	private Component getContactStats() {
		Span stats = new Span(CRM_SERVICE.countContacts() + "contacts");
		stats.addClassNames("text-xl", "mt-m");
		return stats;
	}

	private Chart getCompaniesChart() {
		Chart chart = new Chart(ChartType.PIE);

		DataSeries dataSeries = new DataSeries();
		CRM_SERVICE.findAllCompanies().forEach(company ->
			                                      dataSeries.add(new DataSeriesItem(company.getName(),
				                                     company.getEmployeeCount())));
		chart.getConfiguration().setSeries(dataSeries);
		return chart;
	}
}
